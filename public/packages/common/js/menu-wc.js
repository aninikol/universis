'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">Universis Project</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="license.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>LICENSE
                            </a>
                        </li>
                        <li class="link">
                            <a href="dependencies.html" data-type="chapter-link">
                                <span class="icon ion-ios-list"></span>Dependencies
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AuthModule-37831c0f571f457809eed4ad7bc99010"' : 'data-target="#xs-components-links-module-AuthModule-37831c0f571f457809eed4ad7bc99010"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AuthModule-37831c0f571f457809eed4ad7bc99010"' :
                                            'id="xs-components-links-module-AuthModule-37831c0f571f457809eed4ad7bc99010"' }>
                                            <li class="link">
                                                <a href="components/AuthCallbackComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthCallbackComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LogoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LogoutComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-37831c0f571f457809eed4ad7bc99010"' : 'data-target="#xs-injectables-links-module-AuthModule-37831c0f571f457809eed4ad7bc99010"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-37831c0f571f457809eed4ad7bc99010"' :
                                        'id="xs-injectables-links-module-AuthModule-37831c0f571f457809eed4ad7bc99010"' }>
                                        <li class="link">
                                            <a href="injectables/AuthenticationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthenticationService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthRoutingModule.html" data-type="entity-link">AuthRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ErrorModule.html" data-type="entity-link">ErrorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ErrorModule-bf08a197d642df2598377a1eb1875224"' : 'data-target="#xs-components-links-module-ErrorModule-bf08a197d642df2598377a1eb1875224"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ErrorModule-bf08a197d642df2598377a1eb1875224"' :
                                            'id="xs-components-links-module-ErrorModule-bf08a197d642df2598377a1eb1875224"' }>
                                            <li class="link">
                                                <a href="components/ErrorBaseComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ErrorBaseComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HttpErrorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HttpErrorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ErrorRoutingModule.html" data-type="entity-link">ErrorRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' : 'data-target="#xs-components-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' :
                                            'id="xs-components-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' }>
                                            <li class="link">
                                                <a href="components/DialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MsgboxComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MsgboxComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SpinnerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SpinnerComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' : 'data-target="#xs-injectables-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' :
                                        'id="xs-injectables-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' }>
                                        <li class="link">
                                            <a href="injectables/ConfigurationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ConfigurationService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LoadingService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LoadingService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ModalService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ModalService</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' : 'data-target="#xs-pipes-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' :
                                            'id="xs-pipes-links-module-SharedModule-265a13934bbac940d7692a07881c4544"' }>
                                            <li class="link">
                                                <a href="pipes/LocalizedDatePipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LocalizedDatePipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/ApiError.html" data-type="entity-link">ApiError</a>
                            </li>
                            <li class="link">
                                <a href="classes/ProfileNotFoundError.html" data-type="entity-link">ProfileNotFoundError</a>
                            </li>
                            <li class="link">
                                <a href="classes/RequestNotFoundError.html" data-type="entity-link">RequestNotFoundError</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserProfileNotFoundError.html" data-type="entity-link">UserProfileNotFoundError</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ErrorService.html" data-type="entity-link">ErrorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ErrorsHandler.html" data-type="entity-link">ErrorsHandler</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ApplicationConfiguration.html" data-type="entity-link">ApplicationConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ApplicationSettingsConfiguration.html" data-type="entity-link">ApplicationSettingsConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AuthCallbackResponse.html" data-type="entity-link">AuthCallbackResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LocalizationSettingsConfiguration.html" data-type="entity-link">LocalizationSettingsConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LocationPermission.html" data-type="entity-link">LocationPermission</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LocationPermissionAccount.html" data-type="entity-link">LocationPermissionAccount</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LocationPermissionTarget.html" data-type="entity-link">LocationPermissionTarget</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RemoteSettingsConfiguration.html" data-type="entity-link">RemoteSettingsConfiguration</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SettingsConfiguration.html" data-type="entity-link">SettingsConfiguration</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});