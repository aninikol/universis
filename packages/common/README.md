## This repo is under development

# @universis/common
[Universis](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

**@universis/shared** package contains common components and services for building client applications for Universis project.
