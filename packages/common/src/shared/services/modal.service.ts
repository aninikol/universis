import { Injectable, EventEmitter} from '@angular/core';
import {BsModalService, ModalOptions} from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DialogComponent } from '../components/modal/dialog.component';
import { ToastrService } from 'ngx-toastr';

/**
 *
 * Displays a Modal window or a type of Notification (based on choice the color changes)
 * @export
 * @class ModalService
 */
@Injectable()
export class ModalService {
  // @ts-ignore
  private modalRef: BsModalRef;
  // @ts-ignore
  public choice: string;
  config: ModalOptions = {
    ignoreBackdropClick: true,
    keyboard: false,
    initialState: {},
    class: 'modal-content-base'
  };

  constructor(private modalService: BsModalService, private toastr: ToastrService) { }

  /**
   * Create a modal with custom html
   * @param {*} template
   */
  openModal(template: any): BsModalRef {
    return this.modalRef = this.modalService.show(template, this.config);
  }

  /**
   *
   *
   * @param {string} title Set Modal Title
   * @param {string} description Set Modal Description
   * @param {boolean} [isAlert] If true Modal is and Alert box with one Confirm Button
   * @returns {Promise<boolean>} Confirmed = True
   * @memberof ModalService
   */
  openDialog(title: string, description: string, isAlert?: boolean): Promise<boolean> {
    const modalConfig = JSON.parse(JSON.stringify(this.config));
    modalConfig.initialState = { 'title': title, 'description': description, 'isAlert': isAlert};
    const answer = this.modalService.show(DialogComponent, modalConfig).content.choice as EventEmitter<boolean>;
    return new Promise<boolean> ((resolve) => {
      answer.subscribe( (res: any) => {
        resolve(res);
      });
    });
  }

  /**
   *
   * Display Success Notification
   * @param {string} title Title of Notification
   * @param {string} description Description of Notification
   * @memberof ModalService
   */
  notifySuccess(title: string, description: string) {
    this.toastr.success(description, title);
  }

  /**
   *
   * Display Error Notification
   * @param {string} title Title of Notification
   * @param {string} description Description of Notification
   * @memberof ModalService
   */
  notifyError(title: string, description: string) {
    this.toastr.error(description, title);
  }

  /**
   *
   * Display Info Notification
   * @param {string} title Title of Notification
   * @param {string} description Description of Notification
   * @memberof ModalService
   */
  notifyInfo(title: string, description: string) {
    this.toastr.info(description, title);
  }

  /**
   *
   * Display Warning Notification
   * @param {string} title Title of Notification
   * @param {string} description Description of Notification
   * @memberof ModalService
   */
  notifyWarning(title: string, description: string) {
    this.toastr.warning(description, title);
  }
}
