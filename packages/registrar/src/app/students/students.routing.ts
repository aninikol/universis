import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentsHomeComponent} from './components/students-home/students-home.component';
import {StudentsTableComponent} from './components/students-table/students-table.component';

const routes: Routes = [
    {
        path: '',
        component: StudentsHomeComponent,
        data: {
            title: 'Students'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: StudentsTableComponent,
                data: {
                    title: 'Students List'
                }
            },
            {
                path: 'active',
                component: StudentsTableComponent,
                data: {
                    title: 'Active Students'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class StudentsRoutingModule {
}
