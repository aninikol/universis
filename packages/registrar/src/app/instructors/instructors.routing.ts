import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstructorsHomeComponent} from './components/instructors-home/instructors-home.component';
import {InstructorsTableComponent} from './components/instructors-table/instructors-table.component';

const routes: Routes = [
    {
        path: '',
        component: InstructorsHomeComponent,
        data: {
            title: 'Instructors'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: InstructorsTableComponent,
                data: {
                    title: 'Instructors List'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class InstructorsRoutingModule {
}
