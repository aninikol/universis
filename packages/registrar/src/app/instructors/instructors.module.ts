import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructorsHomeComponent } from './components/instructors-home/instructors-home.component';
import {InstructorsRoutingModule} from './instructors.routing';
import {InstructorsSharedModule} from './instructors.shared';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import { InstructorsTableComponent } from './components/instructors-table/instructors-table.component';

@NgModule({
  imports: [
      CommonModule,
      TranslateModule,
      InstructorsSharedModule,
      InstructorsRoutingModule,
      TablesModule
  ],
  declarations: [InstructorsHomeComponent, InstructorsTableComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InstructorsModule {

    constructor(private _translateService: TranslateService) {
        //
    }

}
