import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoursesSharedModule} from './courses.shared';
import {CoursesRoutingModule} from './courses.routing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { CoursesHomeComponent } from './components/courses-home/courses-home.component';
import {environment} from '../../environments/environment';
import {AppSidebarService} from '../registrar-shared/services/app-sidebar.service';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        CoursesSharedModule,
        CoursesRoutingModule
    ],
    declarations: [ CoursesHomeComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoursesModule implements OnInit {

    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.error('An error occurred while loading courses module');
            console.error(err);
        });
    }

    async ngOnInit() {
        // create promises chain
        const sources = environment.languages.map(async (language) => {
            const translations = await import(`./i18n/courses.${language}.json`);
            this._translateService.setTranslation(language, translations, true);
        });
        // execute chain
        await Promise.all(sources);
    }

}
