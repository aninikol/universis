import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';

const routes: Routes = [
    {
        path: '',
        component: CoursesHomeComponent,
        data: {
            title: 'Courses'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class CoursesRoutingModule {
}
