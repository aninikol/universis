import {template, at} from 'lodash';
import {TableColumnConfiguration} from './advanced-table.interfaces';

export abstract class AdvancedColumnFormatter implements TableColumnConfiguration {

    public class: string;
    public defaultContent: string;
    public formatString: string;
    public formatter: string;
    public name: string;
    public sortable: boolean;
    public title: string;

    abstract render(data: any, type: any, row: any, meta: any);
}

export class NestedPropertyFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
            return at(row, column.data.replace(/\//g, '.'));
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class LinkFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
            return `<a href="${template(this.formatString)(row)}">${data}</a>`;
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class TemplateFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
            return template(this.formatString)(row);
        }
        return data;
    }
    constructor() {
        super();
    }
}
