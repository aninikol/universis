
export declare interface TableColumnConfiguration {
    name?: string;
    title: string;
    sortable?: boolean;
    class?: string;
    defaultContent?: string;
    formatter?: string;
    formatString?: string;
    hidden?: boolean;
    property?: string;
    order?: string;
}

export declare interface TableConfiguration {
    title?: string;
    model: string;
    searchExpression?: string;
    columns: Array<TableColumnConfiguration>;
}
