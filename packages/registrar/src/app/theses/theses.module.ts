import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThesesHomeComponent } from './components/theses-home/theses-home.component';
import {ThesesSharedModule} from './theses.shared';
import {ThesesRoutingModule} from './theses.routing';

@NgModule({
  imports: [
    CommonModule,
      ThesesSharedModule,
      ThesesRoutingModule
  ],
  declarations: [ThesesHomeComponent]
})
export class ThesesModule { }
