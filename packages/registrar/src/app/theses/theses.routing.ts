import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ThesesHomeComponent} from './components/theses-home/theses-home.component';

const routes: Routes = [
    {
        path: '',
        component: ThesesHomeComponent,
        data: {
            title: 'Theses'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ThesesRoutingModule {
}
