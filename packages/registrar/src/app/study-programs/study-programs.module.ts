import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudyProgramsHomeComponent } from './components/study-programs-home/study-programs-home.component';
import {StudyProgramsRoutingModule} from './study-programs.routing';
import {StudyProgramsSharedModule} from './study-programs.shared';

@NgModule({
  imports: [
    CommonModule,
    StudyProgramsSharedModule,
      StudyProgramsRoutingModule
  ],
  declarations: [StudyProgramsHomeComponent]
})
export class StudyProgramsModule { }
