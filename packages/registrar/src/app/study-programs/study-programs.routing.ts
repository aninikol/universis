import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudyProgramsHomeComponent} from './components/study-programs-home/study-programs-home.component';

const routes: Routes = [
    {
        path: '',
        component: StudyProgramsHomeComponent,
        data: {
            title: 'Study Programs'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class StudyProgramsRoutingModule {
}
